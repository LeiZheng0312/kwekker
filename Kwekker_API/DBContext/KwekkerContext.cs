﻿using Kwekker_API.Models;
using Microsoft.EntityFrameworkCore;

namespace Kwekker_API.DBContext
{
    public class KwekkerContext : DbContext
    {
        public KwekkerContext(DbContextOptions<KwekkerContext> options) : base(options)
        {
            
        }

        public DbSet<User> Users { get; set; } = null!;
        public DbSet<Tweet> Tweets { get; set; } = null!;
    }
}
