﻿using Kwekker_API.Models;
using Microsoft.EntityFrameworkCore;

namespace Kwekker_API.DBContext
{
    public class SeedDB
    {
        public static void Initialize(KwekkerContext context)
        {
            if (!context.Users.Any())
            {
                context.Users.Add(new User() { Username = "First_User", FullName = "User01", AccountCreated = DateTime.UtcNow});
                context.Users.Add(new User() { Username = "Second_User", FullName = "User02"});
                context.Users.Add(new User() { Username = "Third_User", FullName = "User03"});
            }

            context.SaveChanges();
        }
    }
}
