﻿using Kwekker_API.DBContext;
using Kwekker_API.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.Security.Claims;

namespace Kwekker_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TweetController : Controller
    {
        private readonly KwekkerContext _context;

        public TweetController(KwekkerContext context)
        {
            _context = context;
        }

        // POST: api/Tweet
        [HttpPost]
        //[Authorize]
        public async Task<ActionResult> PostTweet(TweetDTO tweet)
        {
            //string userId = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;
            //er user = await _context.Users.Where(u => u.Auth0Id == userId).FirstOrDefaultAsync();

            Tweet newTweet = new Tweet()
            {
                //UserId = user.Id,
                UserId = 4,
                Content = tweet.Content,
                Image = tweet.Image,
                TweetDate = DateTime.UtcNow
            };

            EntityEntry<Tweet> entry = _context.Tweets.Add(newTweet);

            await _context.SaveChangesAsync();

            return Ok("Tweet has been posted");
        }
    }
}
