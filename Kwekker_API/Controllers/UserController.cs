﻿using Kwekker_API.DBContext;
using Kwekker_API.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;

namespace Kwekker_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        //private readonly IManagementApiClient _managementApiClient;
        private readonly KwekkerContext _context;

        public UserController(KwekkerContext context)
        {
            _context = context;
            //_managementApiClient = managementApiClient;
        }

        // GET: api/Users
        [HttpGet]
        public async Task<ActionResult<IEnumerable<User>>> GetUsers()
        {
            if (_context.Users == null)
            {
                return NotFound();
            }
            var users = await _context.Users.ToListAsync();

            return users;
        }
    }
}
