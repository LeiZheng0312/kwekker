﻿namespace Kwekker_API.Models
{
    public class Tweet
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Content { get; set; }
        public byte[]? Image { get; set; }
        public DateTime TweetDate { get; set; }
    }
}
