﻿namespace Kwekker_API.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Auth0Id { get; set; }
        public string Username { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Bio { get; set; }
        public DateTime? BirthDate { get; set; }
        public DateTime AccountCreated { get; set; }

    }
}
