﻿namespace Kwekker_API.Models
{
    public class TweetDTO
    {
        public string Content { get; set; }
        public byte[]? Image { get; set; }
        public DateTime TweetDate { get; set; }
    }
}
