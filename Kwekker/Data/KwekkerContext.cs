﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Kwekker.Models;

namespace Kwekker.Data
{
    public class KwekkerContext : DbContext
    {
        public KwekkerContext (DbContextOptions<KwekkerContext> options)
            : base(options)
        {
        }

        public DbSet<Kwekker.Models.User> User { get; set; } = default!;
    }
}
