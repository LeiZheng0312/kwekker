using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Kwekker.Models;

namespace Kwekker.Pages.Account
{
    public class ProfileModel : PageModel
    {
        public Auth0_User User { get; set; } = default!;

        public void OnGet()
        {
        }
    }
}
