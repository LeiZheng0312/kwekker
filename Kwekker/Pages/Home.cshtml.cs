﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Kwekker.Models;
using Kwekker.Controllers;

namespace Kwekker.Pages
{
    public class HomeModel : PageModel
    {
        public HomeModel()
        {

        }

        [BindProperty]
        public List<User> User { get;set; } = default!;

        public async Task OnGetAsync()
        {
            HomeController homeController = new HomeController();

            User = await homeController.Index();
        }
    }
}
