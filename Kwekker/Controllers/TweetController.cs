﻿using Kwekker.Helper;
using Kwekker.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Text;

namespace Kwekker.Controllers
{
    public class TweetController : Controller
    {
        URI_Helper _api = new URI_Helper();

        public async Task PostTweet(Tweet tweet)
        {
            HttpClient client = _api.Initial();
            HttpContent body = new StringContent(JsonConvert.SerializeObject(tweet), Encoding.UTF8, "application/json");
            var response = await client.PostAsync("api/Tweet", body);

            if (response.IsSuccessStatusCode)
            {
                var content = response.Content.ReadAsStringAsync().Result;

                if (!string.IsNullOrEmpty(content))
                {
                    var createdTweet = JsonConvert.DeserializeObject<Tweet>(content);

                    if (createdTweet != null)
                    {
                        System.Diagnostics.Debug.WriteLine(createdTweet.Id);
                    }
                }
            }
        }
        public IActionResult Index()
        {
            return View();
        }
    }
}
