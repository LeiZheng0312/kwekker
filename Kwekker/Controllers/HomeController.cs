﻿using Kwekker.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Kwekker.Helper;

namespace Kwekker.Controllers
{
    public class HomeController : Controller
    {
        URI_Helper _api = new URI_Helper();

        public async Task<List<User>> Index()
        {
            List<User> userList = new List<User>();
            HttpClient client = _api.Initial();
            HttpResponseMessage res = await client.GetAsync("api/User");

            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;

                userList = JsonConvert.DeserializeObject<List<User>>(result);
            }

            return userList;
        }

        public async Task<List<User>> Test()
        {
            List<User> userList = new List<User>();
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync("https://localhost:7181/api/User"))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    userList = JsonConvert.DeserializeObject<List<User>>(apiResponse);
                }
            }
            return userList;
        }
    }
}
