﻿namespace Kwekker.Helper
{
    public class URI_Helper
    {
        public HttpClient Initial()
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri("https://localhost:7181/");

            return client; 
        }
    }
}
