﻿namespace Kwekker.Models
{
    public class Auth0_User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string EmailAddress { get; set; }
    }
}
