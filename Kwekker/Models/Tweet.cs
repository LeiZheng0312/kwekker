﻿namespace Kwekker.Models
{
    public class Tweet
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Content { get; set; }
        public IFormFile? Image { get; set; }
        public DateTime TweetDate { get; set; }
    }
}
